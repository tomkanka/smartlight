package pl.test.smartlight;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

//@Component
public class IpAuthenticationProvider implements AuthenticationProvider {
     
	
	
	@Autowired
	private UserDetailsService userDetailsService;
	
   Set<String> whitelist = new HashSet<String>();
 
    public IpAuthenticationProvider() {
        whitelist.add("127.0.0.1");
        whitelist.add("12.12.12.12");
    }
 
    @Override
    public Authentication authenticate(Authentication auth) throws AuthenticationException {
        WebAuthenticationDetails details = (WebAuthenticationDetails) auth.getDetails();
        String userIp = details.getRemoteAddress();
        if(! whitelist.contains(userIp)){
            throw new BadCredentialsException("Invalid IP Address");
        }
        
        Collection<SimpleGrantedAuthority> grantedAuthorities= new ArrayList<SimpleGrantedAuthority>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE1"));
		Object principal = userDetailsService.loadUserByUsername("user");
		Object credentials = "";
		return new UsernamePasswordAuthenticationToken(principal, credentials, grantedAuthorities);
    }

	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return false;
	}
}
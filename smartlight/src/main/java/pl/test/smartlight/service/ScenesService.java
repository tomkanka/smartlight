package pl.test.smartlight.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.test.smartlight.dto.DeviceDTO;
import pl.test.smartlight.dto.SceneDTO;
import pl.test.smartlight.dto.SceneDTO.SceneDTOBuilder;
import pl.test.smartlight.dto.StateDTO;
import pl.test.smartlight.model.Device;
import pl.test.smartlight.model.DeviceSetup;
import pl.test.smartlight.model.Scene;
import pl.test.smartlight.repository.DeviceRepository;
import pl.test.smartlight.repository.SceneRepository;

@Service
public class ScenesService {

//	private List<SceneDTO> scenes = new ArrayList<SceneDTO>();
	
	@Autowired
	private SceneRepository sceneRepository;
	
	@Autowired
	private DeviceRepository deviceRepository;
	
	public List<SceneDTO> findAll()
	{
		List<SceneDTO> result = new ArrayList<SceneDTO>();
		
		for(Scene scene : sceneRepository.findAll())
		{
			SceneDTO dto = copyToDTO(scene);
			
			result.add(dto);
		};
		
		
		return result ;
	}

	private SceneDTO copyToDTO(Scene scene) {

		SceneDTOBuilder dtoBuilder = SceneDTO.builder()
				.id(scene.getId())
				.name(scene.getName())
				.createdAt(scene.getCreatedAt());
		
		for(DeviceSetup ds : scene.getDeviceSetups())
		{
			 DeviceDTO stateDTO = DeviceDTO.builder()
					.name(ds.getDevice().getName())
					.uri(ds.getDevice().getUrl())
					.type(ds.getDevice().getType())
					.state(StateDTO.builder()
							.brightness(ds.getBrightness())
							.color(ds.getColor())
							.on(ds.isOn())
							.build())
					.build();

			dtoBuilder.deviceState(stateDTO);
		}
		return dtoBuilder.build();
	}
	
	public SceneDTO findById(Long id) {
		Optional<Scene> scene = sceneRepository.findById(id);
		if (! scene.isPresent())
			return null;
		
		return copyToDTO(scene.get());
	}

	public void deleteById(Long id) {
		sceneRepository.deleteById(id);
	}
	
	public void saveScene(SceneDTO dto)
	{
		Optional<Scene> scene = sceneRepository.findByName(dto.getName());
		if (scene.isPresent())
			sceneRepository.delete(scene.get());
		
		Scene newScene = new Scene();
		newScene.setName(dto.getName());
		newScene.setCreatedAt(new Date());
		newScene.setDeviceSetups(new HashSet<DeviceSetup>());
		
		for(DeviceDTO dsDTO : dto.getDeviceStates())
		{
			if (dsDTO.getState() == null)
				continue;
			
			Optional<Device> device = deviceRepository.findById(dsDTO.getUri());
			if (! device.isPresent())
			{
				Device newDevice = new Device();
				newDevice.setUrl(dsDTO.getUri());
				newDevice.setName(dsDTO.getName());
				newDevice.setType(dsDTO.getType());
				
				deviceRepository.save(newDevice);
				device = deviceRepository.findById(dsDTO.getUri());
			}
			
			DeviceSetup ds = new DeviceSetup();
			ds.setDevice(device.get());
			ds.setBrightness(dsDTO.getState().getBrightness());
			ds.setColor(dsDTO.getState().getColor());
			ds.setOn(dsDTO.getState().isOn());
			
			newScene.getDeviceSetups().add(ds);
		}
		
		sceneRepository.save(newScene);
	}
	
	
}

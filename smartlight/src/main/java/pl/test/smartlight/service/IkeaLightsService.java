package pl.test.smartlight.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.elements.exception.ConnectorException;
import org.eclipse.californium.scandium.DTLSConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.test.smartlight.dto.DeviceDTO;
import pl.test.smartlight.dto.DeviceDTO.DeviceDTOBuilder;
import pl.test.smartlight.dto.StateDTO;
import pl.test.smartlight.model.DeviceType;
import pl.test.smartlight.model.IkeaColor;

//@Service
public class IkeaLightsService implements LightsService {

	private static final Logger log = LoggerFactory.getLogger(IkeaLightsService.class);

	
	private final String FIELD_NAME = "9001";
	private final String FIELD_TYPE = "5750";
	private final String FIELD_STATE = "3311";
	
	private final String STATE_ON = "5850";
	private final String STATE_BRIGHTNESS = "5851";
	private final String STATE_COLOR = "5706";
	private final String STATE_COLOR_TEMPERATURE = "5711";
	private final String STATE_TRANSITION_TIME = "5712";
	
	@Autowired
	private CoapDtlsSupport dtsl;

	@Value("${coap.url}")
	private String baseUrl;
	
	@Value("${coap.identity}")
	private String coapIdentity;
	
	@Value("${coap.presharedKey}")
	private String coapPresharedKey;
	
	private CoapClient coapClient;

	
	private List<DeviceDTO> bulbs = new ArrayList<DeviceDTO>();
	
	@PostConstruct
	public void init()
	{
		this.coapClient = new CoapClient(baseUrl);

		DTLSConnector dtlsConnector = dtsl.createConnector(this.coapIdentity, this.coapPresharedKey);

		CoapEndpoint coapEndpoint = new CoapEndpoint.Builder()
					.setNetworkConfig(NetworkConfig.getStandard()
							.set(NetworkConfig.Keys.TCP_CONNECTION_IDLE_TIMEOUT, 60 * 10)
							.set(NetworkConfig.Keys.DTLS_AUTO_RESUME_TIMEOUT, 1000 * 60 * 30))
					.setConnector(dtlsConnector).build();
		this.coapClient.setEndpoint(coapEndpoint);
		
		try
		{
		Set<WebLink> resources = this.coapClient.discover("");
		for(WebLink wl : resources)
		{
			if (! wl.getURI().startsWith("//15001/"))
				continue;
			
			Map<String, Object> map = getJson(wl.getURI());
			DeviceType type = deviceType((Integer) map.get(FIELD_TYPE));
			
			
			DeviceDTOBuilder bulbBuilder = DeviceDTO.builder();
			bulbBuilder.uri(wl.getURI())
				.type(type)
				.name((String) map.get(FIELD_NAME));

			StateDTO state = getState(map);
			bulbBuilder.state(state);
			
			
			
			this.bulbs.add(bulbBuilder.build());
	    	log.debug(wl.getURI() + " - " + map.get("9001"));
			
		}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private StateDTO getState(Map<String, Object> map) {
		List<Map<String, Object>> o = (List<Map<String, Object>>) map.get(FIELD_STATE);
		if (o != null)
		{
			Map<String, Object> state = o.get(0);
			log.debug(state.toString());
			StateDTO stateDTO = StateDTO.builder()
				.on(state.get(STATE_ON).equals(Integer.valueOf(1)))
				.color(IkeaColor.findByColor((String) state.get(STATE_COLOR)))
				.brightness((Integer) state.get(STATE_BRIGHTNESS))
				.colorTemperature((Integer)state.get(STATE_COLOR_TEMPERATURE))
//				.transitionTime((Integer)state.get(STATE_TRANSITION_TIME))
				.build();
			return stateDTO;
		}
		return null;
	}

	private StateDTO getState(String uri) {
		try
		{
			Map<String, Object> map = getJson(uri);
			return getState(map);
		}
		catch (Exception e) {
			return null;
		}
	}

	
	private String putJson(String path, String payload) throws ConnectorException, IOException {
		this.coapClient.setURI(this.baseUrl + path);
		CoapResponse response = coapClient.put(payload, MediaTypeRegistry.APPLICATION_JSON);
		return response.isSuccess() ? "OK" : "FAILED";
	}
	
	private Map<String, Object> getJson(String url) throws ConnectorException, IOException {
		this.coapClient.setURI(this.baseUrl + url);

		CoapResponse response = this.coapClient.get();
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> deviceMap = mapper.readValue(response.getResponseText(), Map.class);
		
		return deviceMap;
	}

	
	
	private DeviceType deviceType(Integer typeCode) {
		switch (typeCode) {
		case 0:
			return DeviceType.SWITCH;
		case 2:
			return DeviceType.LIGHT;
		case 3:
			return DeviceType.SMART_PLUG;
		case 4:
			return DeviceType.SENSOR;
		}
		return DeviceType.UNKNOWN;
	}


	@Override
	public List<DeviceDTO> getDeviceList()
	{
		updateBulbs();
		return this.bulbs;
	}

	private void updateBulbs() {
		synchronized (this.bulbs) {
			for(DeviceDTO d : this.bulbs)
			{
				StateDTO state = getState(d.getUri());
				d.setState(state);
			}
		}
	}

	@Override
	public DeviceDTO getDevice(String uri) {
		for(DeviceDTO d : this.bulbs)
		{
			if (d.getUri().equals(uri))
			{
				StateDTO state = getState(d.getUri());
				d.setState(state);
				return d;
			}
		}
		return null;
	}

	@Override
	public void setupDevice(DeviceDTO device) {
		if (device.getType() == DeviceType.LIGHT)
			setupLight(device);
			
	}

	private void setupLight(DeviceDTO device) {

		try {

			StringBuffer fields = new StringBuffer();
			
			int on = (device.getState().isOn() ? 1 : 0);
			fields.append(setupValue(STATE_ON, on));

			if (on == 1 ) // Nie wysyłamy nic jeśli wyłączamy 
			{
				if (device.getState().getBrightness() != null)
					fields.append(setupValue(STATE_BRIGHTNESS, device.getState().getBrightness()));

				if (device.getState().getColor() != null)
					fields.append(setupValue(STATE_COLOR, device.getState().getColor().getColor()));

//				if (device.getState().getColorTemperature() != null)
//					fields.append(setupValue(STATE_COLOR_TEMPERATURE, device.getState().getColorTemperature()));
			}
			fields.append(setupValue(STATE_TRANSITION_TIME, 40));
			fields.delete(fields.length() - 2, fields.length());

			String payload = String.format("{\"%s\":[ { %s } ] }", FIELD_STATE, fields);
			String res = putJson(device.getUri(), payload);
			log.debug(res + " : " + payload);

		} catch (ConnectorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	private String setupValue(String field, Object value) throws ConnectorException, IOException {
		String f = "";
		if (value instanceof Number)
			f = String.format("\"%s\":%s , ", field, value);
		else 
			f = String.format("\"%s\":\"%s\" , ", field, value);
			
		return f;
	}
	
}

package pl.test.smartlight.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.eclipse.californium.scandium.DTLSConnector;
import org.eclipse.californium.scandium.config.DtlsConnectorConfig;
import org.eclipse.californium.scandium.dtls.pskstore.StaticPskStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author Christian Tzolov
 */
@Component
public class CoapDtlsSupport {

	private static final Logger log = LoggerFactory.getLogger(CoapDtlsSupport.class);

	@Value("${coap.key-store-location}")
	private String keyStoreLocation;

	@Value("${coap.trust-store-password}")
	private String trustStorePassword;

	@Value("${coap.trust-store-location}")
	private String trustStoreLocation;

	@Value("${coap.key-store-password}")
	private String keyStorePassword;

	@Value("${coap.key-store-alias}")
	private String keyStoreAlias;

	public DTLSConnector createConnector(String identity, String preSharedKey) {

		DTLSConnector dtlsConnector = null;
		try {
			// load Java trust store
			Certificate[] trustedCertificates = loadTrustStore();

			// load client key store
			KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(getInputStream(this.keyStoreLocation), this.keyStorePassword.toCharArray());

			// Build DTLS config
			DtlsConnectorConfig.Builder builder = new DtlsConnectorConfig.Builder()
					.setAddress(new InetSocketAddress(0))
					.setIdentity((PrivateKey) keyStore.getKey(this.keyStoreAlias, this.keyStorePassword.toCharArray()),
							keyStore.getCertificate(this.keyStoreAlias).getPublicKey())
					.setTrustStore(trustedCertificates)
					.setMaxConnections(100)
					.setStaleConnectionThreshold((long)24 * 60 * 60);

			if (StringUtils.hasText(identity) && StringUtils.hasText(preSharedKey)) {
				builder.setPskStore(new StaticPskStore(identity, preSharedKey.getBytes()));
			}

			// Create DTLS endpoint
			dtlsConnector = new DTLSConnector(builder.build());
			dtlsConnector.setRawDataReceiver(raw -> log.debug(new String(raw.getBytes())));
		}
		catch (Exception e) {
			log.error("Error creating DTLS endpoint");
			e.printStackTrace();
		}

		return dtlsConnector;
	}

	private Certificate[] loadTrustStore() throws Exception {
		// load client key store
		KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(getInputStream(this.trustStoreLocation), this.trustStorePassword.toCharArray());

		// load trustStore
		TrustManagerFactory trustMgrFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		trustMgrFactory.init(trustStore);
		TrustManager trustManagers[] = trustMgrFactory.getTrustManagers();
		X509TrustManager defaultTrustManager = null;

		for (TrustManager trustManager : trustManagers) {
			if (trustManager instanceof X509TrustManager) {
				defaultTrustManager = (X509TrustManager) trustManager;
			}
		}

		return (defaultTrustManager == null) ? null : defaultTrustManager.getAcceptedIssuers();
	}

	private InputStream getInputStream(String resourceUri) throws IOException {
		return new DefaultResourceLoader().getResource(resourceUri).getInputStream();
	}

}
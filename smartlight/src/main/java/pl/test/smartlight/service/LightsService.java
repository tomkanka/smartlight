package pl.test.smartlight.service;

import java.util.List;

import pl.test.smartlight.dto.DeviceDTO;

public interface LightsService {

	List<DeviceDTO> getDeviceList();

	DeviceDTO getDevice(String uri);

	void setupDevice(DeviceDTO device);

}
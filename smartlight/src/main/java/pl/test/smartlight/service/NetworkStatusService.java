package pl.test.smartlight.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pl.test.smartlight.dto.LuciConnection;
import pl.test.smartlight.dto.LuciConnections;
import pl.test.smartlight.dto.LuciLease;
import pl.test.smartlight.dto.LuciLeases;

@Service
public class NetworkStatusService {


	private static final Logger log = LoggerFactory.getLogger(NetworkStatusService.class);

	
	
	@Value("#{${monitored.macs}}")
	private Map<String, String> monitoredMacs;
	
	@Autowired
	private RestTemplate restTemplate;
	
	private final HttpClient httpClient = HttpClients.createDefault();;
	
	private Date prevTime;
	private Map<String, List<LuciConnection>> prevConnectionsByMac;

	private Map<String, Long> currentTrafficByName = new HashMap<String, Long>();

	
	private Pattern stokPattern = Pattern.compile("stok=([^/]+)");
	private Pattern sysauthPattern = Pattern.compile("sysauth=([^;]+)");


	private String stok;

	private String sysauthCookie;
	
	@PostConstruct
	public void init() throws ClientProtocolException, IOException
	{
		
		retreshRouterToken();
		
        this.prevTime = new Date();
//        this.prevConnectionsByMac = readCurrentConnectionsByMac();
    }
	
	
	@Scheduled(fixedDelay = 15 * 60 * 1000 ) // co 15 minut
	public void retreshRouterToken() throws ClientProtocolException, IOException {
		List<NameValuePair> form = new ArrayList<>();
        form.add(new BasicNameValuePair("luci_username", "root"));
        form.add(new BasicNameValuePair("luci_password", "AntykwAriAt2014"));
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);

        HttpPost httpPost = new HttpPost("http://192.168.11.1/cgi-bin/luci");
        httpPost.setEntity(entity);

        // Create a custom response handler
        ResponseHandler<Boolean> responseHandler = response -> {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status <= 302) {
                HttpEntity responseEntity = response.getEntity();
                
                Header[] headers = response.getHeaders("Set-Cookie");
                for(Header h : headers)
                {
                	Matcher m2 = sysauthPattern.matcher(h.getValue());
                	if (m2.find())
                		NetworkStatusService.this.sysauthCookie = m2.group(1);
                }

                if (responseEntity == null)
                	return null;
                
                Matcher m = stokPattern.matcher(EntityUtils.toString(responseEntity));
                
                if (m.find())
                {
                	NetworkStatusService.this.stok = m.group(1);
                	return true;
                }

                return false;
            } 
            else
            	throw new ClientProtocolException("Unexpected response status: " + status);
            
        };
        httpClient.execute(httpPost, responseHandler);
	}



	private List<LuciConnection> readNetworkConnections()
	{
		String url = String.format("http://192.168.11.1/cgi-bin/luci/;stok=%s/admin/status/realtime/connections_status?_=%f", this.stok, Math.random());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", "sysauth="+this.sysauthCookie);
		org.springframework.http.HttpEntity<String> reqestEntity = new org.springframework.http.HttpEntity<String>(headers);
		ResponseEntity<LuciConnections> resp = restTemplate.exchange(url, HttpMethod.GET, 
				reqestEntity , LuciConnections.class);

		List<LuciConnection> liveConnections = resp.getBody().getConnections();
		return liveConnections;
	}
	
	private List<LuciLease> readDhcpLeases()
	{
		String url = String.format("http://192.168.11.1/cgi-bin/luci/;stok=%s/admin/status/overview?status=1&_=%f", this.stok, Math.random());
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Cookie", "sysauth="+this.sysauthCookie);
		org.springframework.http.HttpEntity<String> reqestEntity = new org.springframework.http.HttpEntity<String>(headers);
		ResponseEntity<LuciLeases> resp = restTemplate.exchange(url, HttpMethod.GET, 
				reqestEntity , LuciLeases.class);

		return resp.getBody().getLeases();
	}
	
	public boolean isNetflixRunning() throws ClientProtocolException, IOException
	{
		Collection<LuciConnection> liveConnections = readNetworkConnections();
		
		List<LuciConnection> tvConnectionsToNetflix = liveConnections.stream()
				.filter(c -> "192.168.11.187".equals(c.getSrc()))
				.filter(c -> isNetflixHost(c.getDst()))
				.filter(c -> ("443".equals(c.getDport()) || "80".equals(c.getDport())))
				.sorted(new Comparator<LuciConnection>() {

					@Override
					public int compare(LuciConnection o1, LuciConnection o2) {
						return -1 * o1.getBytes().compareTo(o2.getBytes());
					}
				})
				.collect(Collectors.toList());

		
		
		List<LuciConnection> testConnections = liveConnections.stream()
				.filter(c -> "192.168.11.187".equals(c.getSrc()))
				.filter(c -> ("443".equals(c.getDport()) || "80".equals(c.getDport())))
				.collect(Collectors.toList());

		long sumBytes = 0;
		long sumPackets = 0;
		for(LuciConnection lc : testConnections)
		{
			sumBytes = sumBytes + lc.getBytes();
			sumPackets = sumPackets + lc.getPackets();
		}

		log.info(String.format("Bytes: %s, packets: %d, per pack: %s", 
				humanReadableByteCount(sumBytes, false), 
				sumPackets,
				(sumPackets > 0) ? humanReadableByteCount(sumBytes / sumPackets, false) : "-"));
		
		return ! tvConnectionsToNetflix.isEmpty() || (sumPackets > 2000);
	}


	private boolean isNetflixHost(String dst) {
		if (dst == null)
			return false;
		
		return dst.startsWith("45.57.");
	}
	
	
	public static String humanReadableByteCount(long bytes, boolean si) {
	    int unit = si ? 1000 : 1024;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));
	    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + "";
	    String s = String.format("%.1f", bytes / Math.pow(unit, exp))
	    		+ pre + "B";
	    return s;
//	    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}
	
	

	// connectionsByMac
	private Map<String, List<LuciConnection>> readCurrentConnectionsByMac()
	{
		Map<String, String> dhcpMap = readDhcpLeases().stream()
			.filter(c -> monitoredMacs.keySet().contains(c.getMacaddr().toLowerCase()))
			.collect(Collectors.toMap(LuciLease::getIpaddr, LuciLease::getMacaddr));

		List<LuciConnection> connetions = readNetworkConnections();

		Map<String, List<LuciConnection>> result = connetions.stream()
			.filter(c ->  dhcpMap.containsKey(c.getSrc()))
			.collect(Collectors.groupingBy( c -> dhcpMap.get(c.getSrc())));

		
		return result;
		
	}

	@Scheduled(fixedDelay = 1 * 60 * 1000)  // co 1 minutę
	public void countNetTraffic()
	{
		if (prevConnectionsByMac == null)
			return;
		
		Date now = new Date();
		Map<String, List<LuciConnection>> connections = readCurrentConnectionsByMac();
		
		this.currentTrafficByName.clear();
		for(String mac : connections.keySet())
		{
			long bytes = 0;
			
			List<LuciConnection> prevConnections = this.prevConnectionsByMac.get(mac);
			
			for(LuciConnection lc : connections.get(mac))
			{
				bytes = bytes + lc.getBytes();
				
				if (prevConnections != null)
				{
					for(LuciConnection prevlc : prevConnections)
					{
						if (theSameConnection(lc, prevlc))
							bytes = bytes - prevlc.getBytes();
					}
				}
			}
			
			this.currentTrafficByName.put(monitoredMacs.get(mac), (1000 * bytes / (now.getTime() - prevTime.getTime())));
		}
		
		this.prevConnectionsByMac = connections;
		this.prevTime  = now;
		
		log.info(this.currentTrafficByName.toString());
		
	}

	
	private boolean theSameConnection(LuciConnection lc1, LuciConnection lc2) {
		if ((lc1 == null) || (lc2 == null))
			return false;
		
		if (! lc1.getSrc().equals(lc2.getSrc()))
			return false;

		if (! lc1.getDst().equals(lc2.getDst()))
			return false;

		if (! lc1.getDport().equals(lc2.getDport()))
			return false;

		if (! lc1.getSport().equals(lc2.getSport()))
			return false;

		return true;
	}



	public Map<String, Long> getCurrentTrafficByName() {
		return currentTrafficByName;
	}


}

package pl.test.smartlight.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import pl.test.smartlight.dto.DeviceDTO;
import pl.test.smartlight.dto.StateDTO;
import pl.test.smartlight.model.DeviceType;
import pl.test.smartlight.model.IkeaColor;

//@Service
public class FakeLightsService implements LightsService{

	private List<DeviceDTO> devices;

	public FakeLightsService() {
		
		DeviceDTO d0 = DeviceDTO.builder().name("Switch").type(DeviceType.SWITCH).uri("//1234/11")
				.build();

		DeviceDTO d1 = DeviceDTO.builder().name("Bulb white").type(DeviceType.LIGHT).uri("//1234/12")
				.state(StateDTO.builder().brightness(100).build())
				.build();

		DeviceDTO d2 = DeviceDTO.builder().name("Bulb warm").type(DeviceType.LIGHT).uri("//1234/13")
				.state(StateDTO.builder().brightness(100).color(IkeaColor.Blue).colorTemperature(100).build())
				.build();

		DeviceDTO d3 = DeviceDTO.builder().name("Bulb color").type(DeviceType.LIGHT).uri("//1234/15")
				.state(StateDTO.builder().brightness(100).color(IkeaColor.Dark_Peach).build())
				.build();

		devices = new ArrayList<DeviceDTO>(Arrays.asList(d0, d1, d2, d3));
	}
	
	@Override
	public List<DeviceDTO> getDeviceList() {
		return this.devices;
	}

	@Override
	public DeviceDTO getDevice(String uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setupDevice(DeviceDTO device) {
		// TODO Auto-generated method stub
		
	}

}

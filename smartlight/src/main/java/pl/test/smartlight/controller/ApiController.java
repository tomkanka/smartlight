package pl.test.smartlight.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.test.smartlight.dto.DeviceDTO;
import pl.test.smartlight.dto.SceneDTO;
import pl.test.smartlight.model.IkeaColor;
import pl.test.smartlight.service.LightsService;
import pl.test.smartlight.service.ScenesService;

@Controller
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.OPTIONS})
@RequestMapping("/api")
public class ApiController {

	@Autowired
	private LightsService lightsService;

	@Autowired
	private ScenesService scenesService;


	
	private SceneDTO currentScene = null;
	private int setupSceneCounter = 0;
	

	@GetMapping("/devices")
	@ResponseBody
	public List<DeviceDTO> devices() {
		List<DeviceDTO> l = lightsService.getDeviceList();
		Collections.sort(l, new Comparator<DeviceDTO>() {

			@Override
			public int compare(DeviceDTO o1, DeviceDTO o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

		return l;
	}

	@GetMapping("/colors")
	@ResponseBody
	public List<Map<String, Object>> colors() {

		List<Map<String, Object>> res = new ArrayList<Map<String,Object>>();
		for(IkeaColor c :  IkeaColor.selectableValues())
		{
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("name", c.name());
			m.put("htmlColor", c.getColor());
			m.put("label", c.getName());
			m.put("warmColdWhite", c.isWarmColdWhite());
			res.add(m);
		}
		return res;
	}


	
	@PostMapping(value="/device/switch/{id}")
	@ResponseBody
	public Boolean deviceSwitch(@PathVariable("id") String id, @RequestBody Map<String, Boolean> map) {
		
		DeviceDTO device =  lightsService.getDevice(id.replace('_', '/'));

		Boolean onoff = map.get("onoff");
		device.getState().setOn(onoff);
		lightsService.setupDevice(device);
		
		return onoff;
	}

	@PostMapping("/device/brightness/{id}")
	@ResponseBody
	public Boolean deviceBrightness(@PathVariable("id") String id, @RequestBody Map<String, Integer> map ) {
		
		DeviceDTO device =  lightsService.getDevice(id.replace('_', '/'));
		Integer brightness = map.get("brightness");
		
		if (device == null)
			return false;
		
		if (brightness == null)
			return false;
		
		device.getState().setBrightness(brightness);
		lightsService.setupDevice(device);
		
		return true;
	}

	@PostMapping("/device/color/{id}")
	@ResponseBody
	public Boolean deviceColor(@PathVariable("id") String id, @RequestBody Map<String, IkeaColor> map ) {
		
		DeviceDTO device =  lightsService.getDevice(id.replace('_', '/'));
		IkeaColor color = map.get("color");
		
		if (device == null)
			return false;
		
		if (color == null)
			return false;
		
		device.getState().setColor(color);
		lightsService.setupDevice(device);
		
		return true;
	}


	@GetMapping("/scenes")
	@ResponseBody
	public List<SceneDTO> scenes() {
		List<SceneDTO> scenes = scenesService.findAll();
		
		if (this.currentScene != null)
			for(SceneDTO s : scenes)
			{
				if (s.getId().equals(this.currentScene.getId()))
					s.setCurrent(true);
			}
		return scenes;
	}


	@PostMapping("/scenes/run/{id}")
	@ResponseBody
	public boolean sceneRun(@PathVariable("id") Long id) {
		SceneDTO scene = scenesService.findById(id);
		this.currentScene = scene;
		this.setupSceneCounter = 4;
		
		setupScene();
		
		return true;
	}


	@Scheduled(fixedDelay = 10000)
	public void setupScene()
	{
		if (this.currentScene == null)
			return;
		
		if (this.setupSceneCounter == 0)
			return;

		this.setupSceneCounter--;
		
//		System.err.println(this.setupSceneCounter + ". " + this.currentScene.getName());
		
		synchronized (this) {
			for(DeviceDTO d : currentScene.getDeviceStates())
			{
				lightsService.setupDevice(d);
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {	}
			
			
		}
	}


}

package pl.test.smartlight.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator;
import com.luckycatlabs.sunrisesunset.dto.Location;

import pl.test.smartlight.dto.DeviceDTO;
import pl.test.smartlight.dto.SceneDTO;
import pl.test.smartlight.dto.StateDTO;
import pl.test.smartlight.model.DeviceType;
import pl.test.smartlight.model.IkeaColor;
import pl.test.smartlight.service.LightsService;
import pl.test.smartlight.service.NetworkStatusService;
import pl.test.smartlight.service.ScenesService;

@Controller
public class MainController {

	@Autowired
	private LightsService lightsService;

	@Autowired
	private ScenesService scenesService;

	@Autowired
	private NetworkStatusService checkNetflixService;

	@Autowired
	private ApiController apiController;

	@GetMapping("/")
	public String index() {
		return "redirect:/device/list";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}

//	@RequestMapping("/logout")
//	public String logout() {
//		return "logout";
//	}

	@GetMapping("/device/list")
	public String deviceList(Model model) {
		List<DeviceDTO> l =  apiController.devices();

		model.addAttribute("bulbs", l);
		return "devices";
	}

	@GetMapping("/device/{id}")
	public String device(@PathVariable("id") String id, Model model) {
		
		DeviceDTO device =  lightsService.getDevice(id.replace('_', '/'));
		model.addAttribute("device", device);
		return "device";
	}

	@GetMapping("/device/switch/{id}")
	public String deviceSwitch(@PathVariable("id") String id, Model model) {
		
		DeviceDTO device =  lightsService.getDevice(id.replace('_', '/'));
		boolean on = device.getState().isOn();
		
		device.getState().setOn(! on);
		lightsService.setupDevice(device);
		
		return "redirect:/device/list";
	}

	@GetMapping("/device/brightness/{id}/{level}")
	@ResponseBody
	public String deviceBrightness(@PathVariable("id") String id, @PathVariable("level") int level ) {
		
		DeviceDTO device =  lightsService.getDevice(id.replace('_', '/'));
		device.getState().setBrightness(level);
		lightsService.setupDevice(device);
		
		return "OK";
	}

	@GetMapping("/device/color/{id}/{color}")
	@ResponseBody
	public String deviceColor(@PathVariable("id") String id, @PathVariable("color") IkeaColor color) {
		
		Map<String, IkeaColor> map = new HashMap<String, IkeaColor>();
		map.put("color", color);
		apiController.deviceColor(id, map );
		
		return "OK";
	}


	
	@GetMapping("/scene/list")
	public String sceneList(Model model) {
		model.addAttribute("scenes", apiController.scenes());
		return "scenes";
	}

	@PostMapping("/scene/add")
	public String sceneAdd(@ModelAttribute SceneDTO newScene) {
		
		List<DeviceDTO> deviceStates = new ArrayList<DeviceDTO>();
		for(DeviceDTO src : lightsService.getDeviceList())
		{
			DeviceDTO nd = src.toBuilder().build();
			deviceStates.add(nd);
		}
		
		newScene.setDeviceStates(deviceStates );
		
		scenesService.saveScene(newScene);
		
		return "redirect:/scene/list";
	}

	@PostMapping("/scene/delete/{id}")
	public String sceneDelete(@PathVariable("id") Long id) {
		
		scenesService.deleteById(id);
		
		return "redirect:/scene/list";
	}

	@GetMapping("/scene/setup/{id}")
	public String sceneSetup(@PathVariable("id") Long id) {

		apiController.sceneRun(id);
		return "redirect:/scene/list";
	}

	private boolean stateTheSame(DeviceDTO d, List<DeviceDTO> currentState) {
		for(DeviceDTO currDev : currentState)
		{
			if (currDev.getUri().equals(d.getUri()))
			{
				System.err.println("==================");
				System.err.println(d.getName());
				return stateTheSame(d.getState(), currDev.getState());
			}
		}
		return false;
	}

	private boolean stateTheSame(StateDTO state1, StateDTO state2) {
		
		System.err.println(state1);
		System.err.println(state2);
		if ( ! state1.isOn() && ! state2.isOn()) // jak są wyłączone to dalej nie sprawdzamy
			return true;
		
		if ( state1.isOn() != state2.isOn()) 
			return false;
		
		if (! state1.getBrightness().equals(state2.getBrightness()))
			return false;
		
		if ( ! (state1.getColor() == null) && ! (state2.getColor() != null))
			if (! state1.getColor().equals(state2.getColor()))
				return false;
		
		return true;
	}

	@GetMapping("/sensors/list")
	public String sensors(Model model) throws ClientProtocolException, IOException {
		
		List<DeviceDTO> devices = lightsService.getDeviceList();
		boolean lightsOn = false;
		for(DeviceDTO dto : devices)
		{
			if (dto.getType() != DeviceType.LIGHT)
				continue;
			
			if (dto.getState().isOn())
			{
				lightsOn = true;
				break;
			}	
		}
		
		
		Location location = new Location("52.25", "21.0");
		SunriseSunsetCalculator calculator = new SunriseSunsetCalculator(location, "Europe/Warsaw");
		Calendar sunrise = calculator.getOfficialSunriseCalendarForDate(Calendar.getInstance());
		Calendar sunset = calculator.getOfficialSunsetCalendarForDate(Calendar.getInstance());
		
		
		Date now = new Date();
		boolean dayTime = (now.after(sunrise.getTime()) && now.before(sunset.getTime()));
		
		model.addAttribute("lightsOn", lightsOn);
		model.addAttribute("dayTime", dayTime);
		model.addAttribute("sunrise", sunrise.getTime().toString());
		model.addAttribute("sunset", sunset.getTime().toString());
		model.addAttribute("streaming", checkNetflixService.isNetflixRunning());
		model.addAttribute("traffic", checkNetflixService.getCurrentTrafficByName());
		
		return "sensors";
	}


	
	@GetMapping("/react")
	public String react(Model model) {
//		List<DeviceDTO> l =  apiController.devices();

//		model.addAttribute("bulbs", l);
		return "react";
	}


	
}

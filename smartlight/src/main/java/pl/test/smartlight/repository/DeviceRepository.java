package pl.test.smartlight.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.test.smartlight.model.Device;

@Repository
public interface DeviceRepository extends CrudRepository<Device, String>{

}

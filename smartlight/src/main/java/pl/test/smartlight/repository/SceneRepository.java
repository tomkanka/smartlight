package pl.test.smartlight.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.test.smartlight.model.Scene;

@Repository
public interface SceneRepository extends CrudRepository<Scene, Long>{

	Optional<Scene> findByName(String name);

}

package pl.test.smartlight;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.test.smartlight.service.FakeLightsService;
import pl.test.smartlight.service.IkeaLightsService;
import pl.test.smartlight.service.LightsService;

@SpringBootApplication

public class SmartLightApp {

	private static final Logger log = LoggerFactory.getLogger(SmartLightApp.class);

//	@Autowired
//	private CheckNetflixService checkNetflixService;

	@Value("${coap.url:}")
	private String coapUrl;
	
	@Value("${tomcat.ajp.port:0}")
	private int ajpPort;

	public static void main(String[] args) {
		SpringApplication.run(SmartLightApp.class, args);
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
		return args -> {
//			log.info(checkNetflixService.toString());
		};
	}

	@Scheduled(fixedDelay = 10000)
	public void check() throws ClientProtocolException, Exception {

//		boolean nr = checkNetflixService.isNetflixRunning();

//		if (nr)
//			lightsService.turnOff();
//		else
//			lightsService.turnOn();
//		log.info(" "+ nr);

	}

	@Bean
	public RestTemplate restTemplate() {
		final RestTemplate restTemplate = new RestTemplate();

		// find and replace Jackson message converter with our own
		for (int i = 0; i < restTemplate.getMessageConverters().size(); i++) {
			final HttpMessageConverter<?> httpMessageConverter = restTemplate.getMessageConverters().get(i);
			if (httpMessageConverter instanceof MappingJackson2HttpMessageConverter) {
				restTemplate.getMessageConverters().set(i, mappingJackson2HttpMessageConverter());
			}
		}

		return restTemplate;
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setObjectMapper(myObjectMapper());
		return converter;
	}

	@Bean
	public ObjectMapper myObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		return mapper;

	}

	@Bean
	public LightsService getLightsService() {
		if (StringUtils.hasText(this.coapUrl))
			return new IkeaLightsService();
		else
			return new FakeLightsService();
	}

	@Bean
	public ServletWebServerFactory servletContainer() {
		TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();

		if (this.ajpPort != 0)
		{
			Connector connector = new Connector("AJP/1.3");
			connector.setScheme("http");
			connector.setPort(this.ajpPort);
			connector.setSecure(false);
			connector.setAllowTrace(false);
        
			tomcat.addAdditionalTomcatConnectors(connector);
		}

		return tomcat;
	}
}

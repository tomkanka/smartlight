
package pl.test.smartlight.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LuciLeases {

	private List<LuciLease> leases;

	public List<LuciLease> getLeases() {
		return leases;
	}

	public void setLeases(List<LuciLease> leases) {
		this.leases = leases;
	}

	
}

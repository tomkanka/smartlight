package pl.test.smartlight.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class LuciConnection {

	private Long bytes;
	private String src;
	private String sport;
	private Long packets; 
	private String dst;
	private String dport;
	
	
	}

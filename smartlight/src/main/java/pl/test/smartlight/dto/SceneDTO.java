package pl.test.smartlight.dto;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SceneDTO {

	private Long id;
	private String name;
	private Date createdAt;
	private boolean current;
	
	@Singular
	private List<DeviceDTO> deviceStates;
}

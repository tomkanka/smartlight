package pl.test.smartlight.dto;

import lombok.Builder;
import lombok.Data;
import pl.test.smartlight.model.DeviceType;

@Data
@Builder(toBuilder = true)
public class DeviceDTO {

	public static enum BulbType {WHITE, COLD_WARM, COLOR};
	
	private String uri;
	private DeviceType type;
	private String name;
	
	private StateDTO state;

	public BulbType getBulbType()
	{
		if (this.type != DeviceType.LIGHT)
			return null;
		
		if (this.state == null)
			return BulbType.WHITE;
		
		if (this.state.getColorTemperature() != null)
			return BulbType.COLD_WARM;
		
		if (this.state.getColor() != null)
			return BulbType.COLOR;
		
		
		return BulbType.WHITE;
			
	}
}


package pl.test.smartlight.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LuciConnections {

	private List<LuciConnection> connections;
//	private List<LuciStatistic> statistics;
	public List<LuciConnection> getConnections() {
		return connections;
	}
	public void setConnections(List<LuciConnection> connections) {
		this.connections = connections;
	}
//	public List<LuciStatistic> getStatistics() {
//		return statistics;
//	}
//	public void setStatistics(List<LuciStatistic> statistics) {
//		this.statistics = statistics;
//	}

	
}

package pl.test.smartlight.dto;

import lombok.Builder;
import lombok.Data;
import pl.test.smartlight.model.IkeaColor;

@Data
@Builder
public class StateDTO {

	private boolean on;
	private Integer brightness;
	private IkeaColor color;
	private Integer colorTemperature;
	private Integer transitionTime;
	
	

}

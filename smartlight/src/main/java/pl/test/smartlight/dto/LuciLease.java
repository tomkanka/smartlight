package pl.test.smartlight.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@NoArgsConstructor
public class LuciLease {

	private String macaddr;
	private String ipaddr;
	private int expires;
}

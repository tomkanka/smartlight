package pl.test.smartlight.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum IkeaColor {

	Blue("4a418a", "Blue"),
	Light_Blue("6c83ba",  "Light Blue"),
	Saturated_Purple("8f2686", "Saturated Purple"),
	Lime("a9d62b", "Lime"),
	Light_Purple("c984bb", "Light Purple"),
	Yellow("d6e44b", "Yellow"),
	Saturated_Pink("d9337c", "Saturated Pink"),
	Dark_Peach("da5d41", "Dark Peach"),
	Saturated_Red("dc4b31", "Saturated Red"),
	Cold_sky("dcf0f8", "Cold sky"),
	Pink("e491af", "Pink"),
	Peach("e57345", "Peach"),
	Warm_Amber("e78834", "Warm Amber"),
	Light_Pink("e8bedd", "Light Pink"),
	Cool_daylight("eaf6fb", "Cool daylight"),
	Candlelight("ebb63e", "Candlelight"),
	Warm_glow("efd275", "Warm glow", true),
	Warm_white("f1e0b5", "Warm white", true),
	Sunrise("f2eccf", "Sunrise"),
	Cool_white("f5faf6", "Cool white", true),
	Other("ffffff", "Other"),
	;
	private final String color;
	private final String name;
	private final boolean warmColdWhite;
	
	private IkeaColor(String color, String name, boolean warmColdWhite)
	{
		this.color = color;
		this.name = name;
		this.warmColdWhite = warmColdWhite;
	}

	private IkeaColor(String color, String name)
	{
		this(color, name, false);
	}

	public String getColor() {
		return color;
	}

	public String getName() {
		return name;
	}

	public boolean isWarmColdWhite() {
		return warmColdWhite;
	}
	
	public static IkeaColor findByColor(String color)
	{
		if (color == null)
			return null;
		
		for(IkeaColor ic : values())
			if (ic.getColor().equals(color))
				return ic;
		
		return Other;
	}
	
	public static List<IkeaColor> selectableValues()
	{
		List<IkeaColor> l = new ArrayList<IkeaColor>(Arrays.asList(IkeaColor.values()));
		l.remove(Other);
		return l;
		
		
	}

	
}

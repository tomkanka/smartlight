package pl.test.smartlight.model;

public enum DeviceType {

	SWITCH, LIGHT, SMART_PLUG,SENSOR, UNKNOWN

}

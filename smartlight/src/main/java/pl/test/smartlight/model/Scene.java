package pl.test.smartlight.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Scene {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column 
	private Date createdAt;
	
	@Column
	private String name;
	
	
	@OneToMany(cascade = CascadeType.ALL)
	private Set<DeviceSetup> deviceSetups;
	
}

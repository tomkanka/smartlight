package pl.test.smartlight.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class DeviceSetup {

	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private Device device;
	
	@Column(name = "on_off")
	private boolean on;
	
	@Column
	private IkeaColor color;
	
	@Column
	private int brightness;
	
	
}

package pl.test.smartlight.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Device {

	@Id
	private String url;
	
	@Column
	private String name;

	@Column
	private DeviceType type;

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DevicesComponent, ColorDialog } from './devices/devices.component';
import { ScenesComponent } from './scenes/scenes.component';

import { MatSliderModule } from '@angular/material/slider';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatCheckboxModule } from '@angular/material/checkbox'; 
import {MatCardModule} from '@angular/material/card'; 

import { ApiUrlInterceptor } from './my.interceptor';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule, MatDialogRef} from '@angular/material/dialog'; 

@NgModule({
  declarations: [
    DevicesComponent,
    NavComponent,
    DashboardComponent,
    ScenesComponent,
    ColorDialog
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatCheckboxModule,
    MatCardModule,
    HttpClientModule,
    MatGridListModule,
    MatMenuModule,
    MatDialogModule
  ],

  providers: [
    //    {provide: CONFIG, useValue: config },
         {provide: HTTP_INTERCEPTORS, useClass: ApiUrlInterceptor, multi: true },
         {provide: MatDialogRef, useValue: ""}
      ],

      bootstrap: [NavComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { SceneDTO } from '../model/scene';
import { CommonModule } from "@angular/common"
import { ColorDTO } from '../model/color';

@Component({
  selector: 'app-scenes',
  templateUrl: './scenes.component.html',
  styleUrls: ['./scenes.component.css']
})
export class ScenesComponent implements OnInit {
  
  scenes: SceneDTO[];
  allColors : ColorDTO[];

  constructor( private apiService : ApiService) {

    this.apiService.scenes().subscribe(res => this.scenes = res);
    this.apiService.colors().subscribe(res => this.allColors = res);
  }


  ngOnInit(): void {
  }

  run(s : SceneDTO)
  {
    this.apiService.runScene(s.id).subscribe(
      res => {alert(res);}
    );
  }

}

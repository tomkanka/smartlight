import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { environment } from '../environments/environment';


@Injectable()

export class ApiUrlInterceptor implements HttpInterceptor {  

    baseUrl = environment.url;

    constructor() {}  
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
        const req = request.clone({
            url: request.url.startsWith("http") ? request.url : this.baseUrl + request.url
        });    
    
        return next.handle(req);
    }
}
import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MatTable } from "@angular/material/table"
import { MatDialog} from '@angular/material/dialog';

import { DataSource } from '@angular/cdk/collections';
import { ApiService} from "../api.service"
import { ColorDTO } from '../model/color';
import { DeviceDTO } from '../model/device';
import { SceneDTO } from '../model/scene';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';


export interface DialogData {
  color: ColorDTO,
  allColors: ColorDTO[];
}

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {


  allColors : ColorDTO[];
  
  displayedColumns= ["name", "type", "onOff", "slider", "button"];
  dataSource = new MyDataSource(this.apiService);

  @ViewChild(MatTable) table: MatTable<any>;


  constructor( private apiService : ApiService, public dialog: MatDialog) {

    this.apiService.colors().subscribe(res => this.allColors= res);
  }

  ngOnInit(): void {
  }


  switch(device : DeviceDTO, o: { checked: any; }) {
    let state = o.checked;

    this.apiService.switch(device.uri, state)
      .subscribe( res => { device.state = state;  /*this.table.renderRows();*/ });
  }

  findColor (name : String) : ColorDTO
  {
    for (let c of this.allColors) {
        if (c.name == name)
             return c;

    }
    return null;
  }

  

  colorLabel(name : String)
  {
    var c = this.findColor(name);
    if (c == null)
        return null;

    return c.label;

  }

  colorHtml(name : String)
  {
    var c = this.findColor(name);
    if (c == null)
        return null;

    return "#"+c.htmlColor;

}

colorClick(element)
{
  let availableColors = new Array();
  let currentColor: ColorDTO;
  for( let c of this.allColors)
  {
    if (element.bulbType == 'COLOR')
    {
      availableColors.push(c)
    }
    else if (element.bulbType == 'COLD_WARM')
    {
      if (c.warmColdWhite)
        availableColors.push(c);
    }

    if (c.name == element.state.color)
      currentColor = c;

  }

  let dialogRef = this.dialog.open(ColorDialog, {
    width: '600px',
    data: {
      color: currentColor,
      allColors: availableColors
    }
  });

  dialogRef.afterClosed().subscribe(newColor => {
    if (newColor)
    {
      this.apiService.setColor(element.uri, newColor)
       .subscribe( res => { 
         if (res)
         {
           element.state.color = newColor.name;
         }
         
        });
    
    }
  });
}

}

export class MyDataSource extends DataSource<any> {

  constructor(private apiService : ApiService) {
    super();
  }


  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<DeviceDTO[]> {
    return this.apiService.getDevices();

  }

  disconnect() {}
}


@Component({
  selector: 'color-dialog',
  templateUrl: 'color-dialog.html',
})
export class ColorDialog {
  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  colorClick(element)
  {
    this.data.color = element;
  }

}
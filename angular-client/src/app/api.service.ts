import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { DeviceDTO } from './model/device';
import { ColorDTO } from './model/color';
import { SceneDTO } from './model/scene';

@Injectable( {
    providedIn: 'root'
} )
export class ApiService {


    constructor( private http: HttpClient ) { }

    getDevices(): Observable<DeviceDTO[]> {
        return this.http.get<DeviceDTO[]>( "/api/devices" );
    }

    colors(): Observable<ColorDTO[]> {
        return this.http.get<ColorDTO[]>( "/api/colors" );
    }
    
    scenes(): Observable<SceneDTO[]> {
        return this.http.get<SceneDTO[]>( "/api/scenes" );
    }
    
    runScene(id : BigInteger): Observable<boolean> {
         const b = this.http.post<boolean>( "/api/scenes/run/" + id, "");
        //  alert("po post " + id);
         return b;
    }
    
    switch( id : string, onoff ): Observable<boolean> {
        let safeUri = id.replace( /\//g, "_" );

        return this.http.post<boolean>( "/api/device/switch/" + safeUri, { onoff } );
    }

    setBrightness( id : string, brightness : number ): Observable<boolean> {
        let safeUri = id.replace( /\//g, "_" );

        return this.http.post<boolean>( "/api/device/brightness/" + safeUri, { brightness } );
    }

    setColor( id, color ): Observable<boolean> {
        let safeUri = id.replace( /\//g, "_" );

        return this.http.post<boolean>( "/api/device/color/" + safeUri, { color: color.name } );
    }


}
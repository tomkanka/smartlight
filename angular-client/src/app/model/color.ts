export interface ColorDTO {
    htmlColor: string;
    label: string;
    name: string;
    warmColdWhite: boolean;
}
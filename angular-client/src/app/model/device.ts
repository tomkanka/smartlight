import {StateDTO} from "./state";

export interface DeviceDTO {
    uri: string;
    type: string;
    name: string;
    state : StateDTO[]
}
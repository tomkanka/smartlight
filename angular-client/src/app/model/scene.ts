import { DeviceDTO } from './device';

export interface SceneDTO {
    id : BigInteger;
    name: string;
    deviceStates : DeviceDTO[];
}
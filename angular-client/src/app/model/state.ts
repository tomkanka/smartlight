export interface StateDTO {
    on : boolean;
    brightness : BigInteger;
    color: string;
}